	<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Kernel;
use App\Http\Controllers\DistrictController;
use App\Http\Controllers\ProvinceController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.login');
})->name('login');
Route::post('/login_process',[Controller::class,'login_process'])->name('login_process');

Route::middleware(['auth'])->group(function(){
	Route::get('pages/index',function (){
		return view('pages.index');
	})->name('index');

	Route::get('/logout','App\Http\Controllers\Controller@logout')->name('logout');
	// -----Province Route-------
	Route::prefix('pages/provinces')->group(function(){
		$ProvinceCtrler = ProvinceController::class;
		Route::get('management',
			[$ProvinceCtrler,'view_all'])
			->name('province.view_all')
			->middleware('can:showProvince,App\Models\Province');
		Route::get('insert',
			[$ProvinceCtrler,'insert_view'])
			->name('province.insert_view',)
			->middleware('can:create,App\Models\Province');
		Route::post('insert_process',
			[$ProvinceCtrler,'insert_process'])
			->name('province.insert_process')
			->middleware('can:create,App\Models\Province');
		Route::get('update_view/{id}',
			[$ProvinceCtrler,'update_view'])
			->name('province.update_view',)
			->middleware('can:edit,App\Models\Province');
		Route::post('update_process/{id}',
			[$ProvinceCtrler,'update_process'])
			->name('province.update_process')
			->middleware('can:edit,App\Models\Province');
		Route::get('delete',
			[$ProvinceCtrler,'delete'])
			->name('province.delete')
			->middleware('can:delete,App\Models\Province');
	});
	// ----District Route------
	Route::prefix('pages/districts')->group(function(){
		$DistrictCtrler = DistrictController::class;
		Route::get('view_all',
			[$DistrictCtrler,'view_all'])
			->name('district.view_all',)
			->middleware('can:showDistrict,App\Models\District');

		Route::get('insert_view',
			[$DistrictCtrler,'insert_view'])
			->name('district.insert_view',)
			->middleware('can:create,App\Models\District');
		Route::post('insert_process',
			[$DistrictCtrler,'insert_process'])
			->name('district.insert_process')
			->middleware('can:create,App\Models\District');
		Route::get('update_view/{id}',
			[$DistrictCtrler,'update_view'])
			->name('district.update_view',)
			->middleware('can:edit,App\Models\District');
		Route::post('update_process/{id}',
			[$DistrictCtrler,'update_process'])
			->name('district.update_process')
			->middleware('can:edit,App\Models\District');
			
		Route::get('delete',
			[$DistrictCtrler,'delete'])
			->name('district.delete')
			->middleware('can:delete,App\Models\District');
	});
	// ----User Route-------
	Route::prefix('pages/users')->group(function(){
		$UserCtrler = UserController::class;
		Route::get('management',
			[$UserCtrler,'view_all'])
			->name('user.view_all')
			->middleware('can:show,App\Models\User');
		Route::get('/get_district',
			[$UserCtrler,'get_district'])
			->name('user.get_district');

		Route::get('insert',
			[$UserCtrler,'insert_view'])
			->name('user.insert_view')
			->middleware('can:create,App\Models\User');
		Route::post('insert_process',
			[$UserCtrler,'insert_process'])
			->name('user.insert_process')
			->middleware('can:create,App\Models\User');
		Route::post('/get_district',
			[$UserCtrler,'get_district'])
			->name('user.get_district');

		Route::get('update_view/{id}',
			[$UserCtrler,'update_view'])
			->name('user.update_view',)
			->middleware('can:edit,App\Models\User');
		Route::post('update_process/{id}',
			[$UserCtrler,'update_process'])
			->name('user.update_process')
			->middleware('can:edit,App\Models\User');
		Route::post('/get_province_update',
			[$UserCtrler,'get_province_update']);

		Route::get('delete',
			[$UserCtrler,'delete'])
			->name('user.delete')
			->middleware('can:delete,App\Models\User');
	});
	// -----Role Route------
	Route::prefix('pages/roles')->group(function(){
		$RoleCtrler = RoleController::class;
		Route::get('management',
			[$RoleCtrler,'view_all'])
			->name('role.view_all')
			->middleware('can:show,App\Models\Role');
		Route::get('detail/{id}',
			[$RoleCtrler,'detail'])
			->name('role.detail')
			->middleware('can:show,App\Models\Role');

		Route::get('insert',
			[$RoleCtrler,'insert_view'])
			->name('role.insert_view')
			->middleware('can:create,App\Models\Role');
		Route::post('insert_process',
			[$RoleCtrler,'insert_process'])
			->name('role.insert_process')
			->middleware('can:create,App\Models\Role');


		Route::get('update_view/{id}',
			[$RoleCtrler,'update_view'])
			->name('role.update_view')
			->middleware('can:edit,App\Models\Role');
		Route::post('update_process/{id}',
			[$RoleCtrler,'update_process'])
			->name('role.update_process')
			->middleware('can:edit,App\Models\Role');

		Route::get('delete',
			[$RoleCtrler,'delete'])
			->name('role.delete')
			->middleware('can:delete,App\Models\Role');
	});
// --------Permission Route----------
	Route::prefix('pages/permissions')->group(function(){
		$PermissionCtrler = PermissionController::class;
		Route::get('management',
			[$PermissionCtrler,'view_all'])
			->name('permission.view_all')
			->middleware('can:showPermission,App\Models\Permission');

		Route::get('insert',
			[$PermissionCtrler,'insert_view'])
			->name('permission.insert_view')
			->middleware('can:create,App\Models\Permission');
		Route::post('insert_process',
			[$PermissionCtrler,'insert_process'])
			->name('permission.insert_process')
			->middleware('can:create,App\Models\Permission');
	
		Route::get('update_view/{id}',
			[$PermissionCtrler,'update_view'])
			->name('permission.update_view')
			->middleware('can:edit,App\Models\Permission');
		Route::post('update_process/{id}',
			[$PermissionCtrler,'update_process'])
			->name('permission.update_process')
			->middleware('can:edit,App\Models\Permission');

		Route::get('delete',
			[$PermissionCtrler,'delete'])
			->name('permission.delete')
			->middleware('can:delete,App\Models\Permission');
	});
});
