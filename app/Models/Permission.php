<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
	protected $table = "permission";
	protected $fillable = [
		'name',
		'description',
		'created_by',
		'updated_by'
	];
	public function user_update()
    {
    	return $this->belongsTo('App\Models\User','updated_by','id');
    }
    public function user()
    {
    	return $this->belongsTo('App\Models\User','created_by','id');
    }
    public function roles()
    {
    	return $this->belongsToMany('App\Models\Role','role_permission','permission_id','role_id');

	}
	// public function roles()
    // {
    //     return $this->belongsToMany('App\Models\Role','role_permission','permission_id','role_id');
    // }
}
