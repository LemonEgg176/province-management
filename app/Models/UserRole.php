<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class UserRole extends Pivot
{
    use HasFactory;
    protected $table = 'user_role';
    public $timestamps = false;
    protected $fillable = [
    	'user_id',
    	'role_id'
    ];

    public function role()
    {
    	return $this->belongsTo('App\Models\Role','role_id','id');
    }
    public function user()
    {
    	return $this->belongsTo('App\Models\User','user_id','id');
    }
}