<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'cs_category_district';
    protected $fillable = [
    	'code',
    	'fullname',
    	'start_date',
    	'province_id'
    ];
    public function province()
    {
    	return $this->belongsTo('App\Models\Province',
            'province_id','code');
    }
    public function user()
    {
        return $this->hasMany('App\Models\User','district_id','code');
    }
}
