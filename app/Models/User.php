<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\District;
use App\Models\Province;
use App\Models\Role;
use App\Models\Permission;
use App\Models\UserRole;
use App\Models\RolePermission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    protected $table = 'users';
    protected $fillable = [
        'email',
        'password',
        'name',
        'province_id',
        'district_id'
    ];

    public function province()
    {

    	return $this->belongsTo('App\Models\Province','province_id','code');
    }
    public function district()
    {

    	return $this->belongsTo('App\Models\District','district_id','code');
    }
    public function roles(){
        return $this->belongsToMany('App\Models\Role','user_role','user_id','role_id')->using('App\Models\UserRole');

    }
        public function hasPermission($permission = null)
    {
        $list = $this->getPermissions();
        if (in_array($permission, $list)) {
           return true;
        }
        return false;
    }
     private function getPermissions()
    {
    // lấy ra tất cả các permission của user hiện tại dưới dạng mảng
//hết
        
        $id = Auth::id();
        $role = User::with('roles.permissions')->where('id',$id)->get();
        $permission = $role[0]->roles[0]->permissions->toArray();
        $permissions_collect = [];
        for ($i=0; $i < count($permission); $i++) {
            array_push($permissions_collect, $permission[$i]['name']);
        }
        // dd($permissions_collect);
        return $permissions_collect;
    }
}
