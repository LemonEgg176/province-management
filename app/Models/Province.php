<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\District;

class Province extends Model
{
    protected $table = 'cs_category_province';
    protected $fillable = [
    	'code',
    	'fullname',
    	'start_date'
    ];


    protected $appends = ['del'];
    public function district()
    {
    	return $this->hasMany('App\Models\District','province_id','code');
    }
    public function user()
    {
        return $this->hasMany('App\Models\User','province_id','code');
    }
   function getDelAttribute()
    {

        $check = ($this->district()->first());
        if(!empty($check)){
            return false;
        }else{
            return true;
        }
    }
}
