<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $table = "roles";
    protected $fillable = [
    	'name',
    	'description',
    	'created_by',
        'updated_by',
        'code'
    ];
    protected $policies = [
    \App\Models\Role::class => \App\Policies\RolePolicy::class,
    ];
    public $timestamps = true;
    public function user()
    {
    	return $this->belongsTo('App\Models\User','created_by','id');
    }
    public function user_update()
    {
    	return $this->belongsTo('App\Models\User','updated_by','id');
    }
    public function users()
    {
        return $this->belongsToMany('App\Models\User','user_role','role_id','user_id')->using('App\Models\UserRole');
    }
    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permission','role_permission','role_id','permission_id');
    }
}
