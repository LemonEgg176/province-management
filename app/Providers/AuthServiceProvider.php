<?php

namespace App\Providers;

use App\Policies\ProvincePolicy;
use App\Policies\DistrictPolicy;
use App\Policies\UserPolicy;
use App\Policies\RolePolicy;
use App\Policies\PermissionPolicy;

use App\Models\Province;
use App\Models\District;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    protected $policies = [
        Province::class => ProvincePolicy::class,
        District::class => DistrictPolicy::class,
        User::class => UserPolicy::class,
        Role::class => RolePolicy::class,
        Permission::class => PermissionPolicy::class
    ];
    public function boot()
    {
        Gate::define('add-role', [RolePolicy::class, 'createRole']);
        Gate::define('edit-role', [RolePolicy::class, 'editRole']);
        Gate::define('delete-role', [RolePolicy::class, 'deleteRole']);
        Gate::define('show-role', [RolePolicy::class, 'showRole']);
        Gate::define('search-role', [RolePolicy::class, 'searchRole']);

        Gate::define('show-district', [DistrictPolicy::class, 'showDistrict']);
        Gate::define('show-province', [ProvincePolicy::class, 'showProvince']);

        // Gate::define('show-district', function ($user, $post) {
        //     return $user->id === $post->user_id;
        // });
    }
}
