<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProvincePolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return(
            $user->hasPermission('add-province')
        );
    }
    public function edit(User $user)
    {
        return(
            $user->hasPermission('edit-province')
        );
    }
    public function delete(User $user)
    {
        return(
            $user->hasPermission('delete-province')
        );
    }
    public function showProvince(User $user)
    {
        return(
            $user->hasPermission('show-province')
        );
    }
    public function search(User $user)
    {
        return(
            $user->hasPermission('search-province')
        );
    }
}
