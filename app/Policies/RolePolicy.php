<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Role;
use App\Models\RolePermission;
use App\Models\Permission;
use App\Models\UserRole;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return(
            $user->hasPermission('add-role')
        );
    }
    public function edit(User $user)
    {
        return (
            $user->hasPermission('edit-role')
        );
    }
    public function delete(User $user)
    {
        return (
            $user->hasPermission('delete-role')
        );
    }
    public function show(User $user)
    {
        return(
            $user->hasPermission('show-role')
        );
    }
    public function search(User $user)
    {
        return(
            $user->hasPermission('search-role')
        );
    }
}
