<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return(
            $user->hasPermission('add-permission')
        );
    }
    public function edit(User $user)
    {
        return(
            $user->hasPermission('edit-permission')
        );
    }
    public function delete(User $user)
    {
        return(
            $user->hasPermission('delete-permission')
        );
    }
    public function showPermission(User $user)
    {
        return(
            $user->hasPermission('show-permission')
        );
    }
    public function search(User $user)
    {
        return(
            $user->hasPermission('search-permission')
        );
    }
}
