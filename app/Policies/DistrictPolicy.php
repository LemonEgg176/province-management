<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DistrictPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return(
            $user->hasPermission('add-district')
        );
    }
    public function edit(User $user)
    {
        return(
            $user->hasPermission('edit-district')
        );
    }
    public function delete(User $user)
    {
        return(
            $user->hasPermission('delete-district')
        );
    }
    public function showDistrict(User $user)
    {
        return(
            $user->hasPermission('show-district')
        );
    }
    public function search(User $user)
    {
        return(
            $user->hasPermission('search-district')
        );
    }
}
