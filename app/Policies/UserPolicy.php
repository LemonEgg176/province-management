<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return(
            $user->hasPermission('add-user')
        );
    }
    public function edit(User $user)
    {
        return(
            $user->hasPermission('edit-user')
        );
    }
    public function delete(User $user)
    {
        return(
            $user->hasPermission('delete-user')
        );
    }
    public function show(User $user)
    {
        return(
            $user->hasPermission('show-user')
        );
    }
    public function search(User $user)
    {
        return(
            $user->hasPermission('search-user')
        );
    }
}
