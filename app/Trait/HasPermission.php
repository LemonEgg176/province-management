<?php
namespace App\Traits;

use App\Models\Role;

trait HasPermissions
{
    protected $permissionList = null;

    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return false;
    }

    public function hasPermission($permission = null)
    {
        if (is_null($permission)) {
            return false;
        }
// check permission có trong mảng , nếu có return true
        $list = $this->getPermissions();

        return false;
    }

    private function getPermissions()
    {
        // 
        // lấy ra tất cả các permission của user hiện tại dưới dạng mảng
    }
}