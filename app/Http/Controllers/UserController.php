<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\District;
use App\Models\User;
use App\Models\Role;
use App\Models\UserRole;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
// use App\Traits\HasPermissions;
// use UxWeb\SweetAlert\SweetAlert;

class UserController extends Controller
{
    protected $table = "users";
    public function view_all(Request $rq)
	{
		$name_search = $rq->name_search;
		$province_search = $rq->province_search;
		$district_search = $rq->district_search;
		$array_province = Province::get();


		$array_district = District::where('province_id',$rq->province_search)
		->get();
		if($name_search !== null 
			|| $province_search !== null)
		{
			$users = User::with(['province','district'])
			->when($name_search,
				function ($query, $name_search)
				{
					return $query->where('name','like',"%$name_search%");
				})
			->when($province_search,
				function ($query, $province_search)
				{
					return $query->where('province_id','=',$province_search);
				})
			->when($district_search,
				function ($query, $district_search)
				{
					return $query->where('district_id','=',$district_search);
				})
			->paginate(20);
		}else{
			$users = User::with(['province','district','roles'])
			->paginate(20);
		}
		return view("pages.users.view_all",
			["users"=>$users,
			"array_district"=>$array_district,
			"array_province"=>$array_province]);
	} 
	public function insert_view()
	{
		$array_district = District::get();
		$array_province = Province::get();
		$roles = Role::get();

		return view("pages.users.insert_view",[
			"array_district"=>$array_district,
			"array_province"=>$array_province,
			"roles"=>$roles
		]);
	}
	public function insert_process(UserRequest $rq)
	{
		$name = $rq->name;
		$email = $rq->email;
		$password = $rq->password;
		$province_id = $rq->province;
		$district_id = $rq->district;
		$role = $rq->role;

		User::create([
			'name' => $name,
			'email' => $email,
			'password' =>Hash::make($password),
			'province_id' =>$province_id,
			'district_id' =>$district_id
		]);

		$max = User::max('created_at');
		// $user_id = DB::select("SELECT id FROM users WHERE created_at
		// 	= (SELECT max(created_at) FROM users) ");
		$user_id = User::where('created_at','=',$max)->get('id')->toArray()[0]['id'];

		UserRole::create([
			'user_id' => $user_id,
			'role_id' => $role
		]);

		return redirect()->route("user.view_all");
	}
	public function get_district(Request $rq)
	{

		$value = $rq->value;
	
		$districts = District::where('province_id',$value)
		->get();
		return response()->json($districts);
	}
	public function update_view($id)
	{
		$user = User::find($id);
		$array_province = Province::get();
		$array_district = District::where('province_id',$user->province_id)->get();
		$roles = Role::get();
		$user_role = UserRole::where('user_id',$id)->get()->toArray()[0]['role_id'];
		return view("pages.users.update_view",
			[
				"user_role"=>$user_role,
				"roles"=>$roles,
				"user"=>$user,
				"array_province"=>$array_province,
				"array_district"=>$array_district
			]);
	}
	public function update_process($id,UserRequest $rq)
	{
		$name = $rq->name;
		$email = $rq->email;
		$province_id = $rq->province;
		$district_id = $rq->district;
		$role = $rq->role;

		User::find($id)->update([
			'name' => $name,
			'email' => $email,
			'province_id' =>$province_id,
			'district_id' =>$district_id
		]);

		UserRole::where('user_id',$id)->delete();
		UserRole::create([
			'role_id' => $role,
			'user_id' => $id
		]);

		return redirect()->route("user.view_all");
	}
	public function get_province_update()
	{
		$value = $_POST['value'];
		$districts = District::where('province_id',$value)
		->get();
		return response()->json($districts);
	}
	public function delete()
	{
		$id = $_GET["id"];
		User::find($id)->delete();
		return redirect()->route("user.view_all");
	}
}
