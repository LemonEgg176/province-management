<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Permission;
use App\Models\User;
use App\Models\RolePermission;
use App\Http\Requests\RoleRequest;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Traits\HasPermissions;

class RoleController extends Controller
{
    protected $table = "roles";
    public function view_all(Request $rq)
    {
    	$name_search = $rq->name_search;
    	$users = User::get();
    	$permissions = Permission::get();
    	if ($name_search !== null) {
            $roles = Role::with('user')
            ->where('name','like',"%$name_search%")
            ->paginate(20);
        }else{
           $roles = Role::with('user')->paginate(20);
        }
    	return view("pages.roles.view_all",
    	['roles'=> $roles,
    	'users' => $users,
    	'permissions' => $permissions
    	]);
    }
    public function detail($id)
    {
		$role = Role::find($id);
		$permissions = Role::find($id)->permissions()->get();
		$parent_permission = Permission::distinct()
		->get(['parent_name','parent']);
		$parent = $permissions->pluck('parent')->toArray();
		$unique_parent = array_unique($parent);
		return view("pages.roles.detail",
			[
				"role"=>$role,
				"permissions"=>$permissions,
				'parent_permission' => $parent_permission,
				'unique_parent' => $unique_parent
			]);
    }
    public function insert_view()
	{
		$users = User::get();
		$roles = Role::get();
		$parent_permission = Permission::distinct()
		->get(['parent_name','parent']);
		$permissions = Permission::get();

		return view("pages.roles.insert_view",
			['roles'=> $roles,
    		'users' => $users,
    		'permissions' => $permissions,
    		'parent_permission' => $parent_permission
    		]);
	}
	public function insert_process(RoleRequest $rq)
	{
		$name = $rq->name;
		$description = $rq->description;
		$created_by = Auth::id();
		$permission = $rq->permission;

		Role::create([
			'name' => $name,
			'description' => $description,
			'created_by' => $created_by,
			'code' => 'egg'
		]);
		$role_id = Role::get('id')->last()->toArray()['id'];

		for ($i=0; $i < count($permission); $i++) { 
			$permission_id = $permission[$i];
			RolePermission::create([
			'role_id' => $role_id,
			'permission_id' => $permission_id
		]);
		}
		return redirect()->route("role.view_all");
	}
	public function update_view($id,Role $role)
	{
		$role = Role::find($id);
		$permissions = Permission::get();
		$parent_permission = Permission::distinct()
		->get(['parent_name','parent']);
		$role_permissions = RolePermission::where('role_id',$id)->get();
		
		return view("pages.roles.update_view",
			[
				'parent_permission' => $parent_permission,
				"role"=>$role,
				"permissions"=>$permissions,
				"role_permissions"=>$role_permissions
			]);
			
	}
	public function update_process($id, RoleRequest $rq)
	{
		$name = $rq->name;
		$description = $rq->description;
		$updated_by = Auth::id();
		$permission = $rq->permission;

		Role::find($id)->update(
			['name'=>$name,
			'description'=>$description,
			'updated_by'=>$updated_by]
		);
		
		RolePermission::where('role_id',$id)->delete();

		for ($i=0; $i < count($permission); $i++) { 
			$permission_id = $permission[$i];

			RolePermission::create([
			'role_id' => $id,
			'permission_id' => $permission_id
		]);
			
		}
		return redirect()->route("role.view_all");
	}
	public function delete()
	{
		$id = $_GET["id"];
		// dd($id);
		Role::find($id)->delete();
		return redirect()->route("role.view_all");
	}
}
