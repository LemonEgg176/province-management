<?php

namespace App\Http\Controllers;
use App\Http\Requests\PemissionRequest;
use App\Models\Permission;
use App\Models\User;
use App\Http\Requests\PermissionRequest;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
	protected $table = "permission";
    public function view_all(Request $rq)
    {
        $name_search = $rq->name_search;
    	$users = User::get();
        if ($name_search !== null) {
            $permissions = Permission::with('user')
            ->where('name','like',"%$name_search%")
            ->paginate(20);
        }else{
            $permissions = Permission::with('user')->paginate(20);
        }
    	return view("pages.permissions.view_all",
    		["permissions" => $permissions,
    		"users" => $users]);
    }
    public function insert_view()
	{
		$permissions = Permission::get();
		return view("pages.permissions.insert_view",[
            "permissions" => $permissions
        ]);
	}
    public function insert_process(PermissionRequest $rq)
    {
        $name = $rq->name;
        $description = $rq->description;
        $created_by = Auth::id();

        Permission::create([
            'name' => $name,
            'description' => $description,
            'created_by' => $created_by
        ]);
        return redirect()->route("permission.view_all");
    }
    public function update_view($id)
    {
        $permission = Permission::find($id);
        return view("pages.permissions.update_view",
        ["permission"=>$permission]);
    }
    public function update_process($id, PermissionRequest $rq)
    {
        $name = $rq->name;
        $description = $rq->description;
        $updated_by = Auth::id();

        Permission::find($id)->update(
            ['name'=>$name,
            'description'=>$description,
            'updated_by'=>$updated_by]
        );
        return redirect()->route("permission.view_all");
    }
    public function delete()
    {
        $id = $_GET['id'];
        Permission::find($id)->delete();
        return redirect()->route("permission.view_all");
    }
}
