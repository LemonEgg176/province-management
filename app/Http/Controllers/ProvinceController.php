<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Province;
use App\Http\Requests\ProvinceRequest;
use Illuminate\Database\Eloquent\Model;

class ProvinceController extends Controller
{
	protected $table = "cs_category_province";
	public function view_all(Request $rq)
	{
		$name_search = $rq->name_search;
		if ($name_search !== null) {
			$array_province = Province::
			where('fullname','like',"%$name_search%")
			->paginate(20);
		}else{
			$array_province = Province::paginate(20);
		}
		return view("pages.provinces.view_all",
			["array_province"=>$array_province]);
	} 
	public function insert_view()
	{
		return view("pages.provinces.insert_view");
	}
	public function insert_process(ProvinceRequest $rq)
	{
		$code = $rq->code;
		$fullname = $rq->fullname;
		$start_date = $rq->start_date;
		Province::create([
			'code' => $code,
			'fullname' => $fullname,
			'start_date' =>$start_date
		]);
		return redirect()->route("province.view_all")->
		with("success","Thêm thành phố thành công");
	}
	public function update_view($id)
	{
		$province = Province::find($id);

		return view("pages.provinces.update_view",
			["province"=>$province]);
	}
	public function update_process($id, ProvinceRequest $rq)
	{
		Province::find($id)->update($rq->all());
		return redirect()->route("province.view_all");
	}
	public function delete()
	{
		$id = $_GET['id'];
		Province::find($id)->delete();
		return redirect()->route("province.view_all");
	}
}
