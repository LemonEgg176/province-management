<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\District;
use App\Http\Requests\DistrictRequest;
// use UxWeb\SweetAlert\SweetAlert;

class DistrictController extends Controller
{
	protected $table = "cs_category_district";
	public function view_all(Request $rq)
	{
		$name_search = $rq->name_search;
		$province_search = $rq->province_search;
		$date_from = $rq->date_from;
		$date_to = $rq->date_to;
		$array_province = Province::get();
		if($name_search !== null 
			|| $province_search !== null
			|| $date_from !== null 
			|| $date_to !== null)
		{
			$array_district 
			= District::
			when($name_search, 
				function($query, $name_search){
					return $query->where('fullname','like',"%$name_search%");
			})
			->when($province_search,
				function($query, $province_search){
					return $query->where('province_id','=',$province_search);
			})
			->when($date_from,
				function($query, $date_from){
					return $query->whereDate('start_date','>=',$date_from);
			})
			->when($date_to,
				function($query, $date_to){
					return $query->whereDate('start_date','<=',$date_to);
			})
			->with("province")
			->paginate(20);
			// dd($array_district->items());
		}else{
			$array_district = District::with('province')
			->paginate(20);
		}
			return view("pages.districts.view_all",
			["array_district"=>$array_district,
			"array_province"=>$array_province]
			);
	} 
	public function insert_view()
	{
		$array_province = Province::get();
		return view("pages.districts.insert_view",[
            "array_province" => $array_province
        ]);
	}
	public function insert_process(DistrictRequest $rq)
	{
		$code = $rq->code;
		$fullname = $rq->fullname;
		$start_date = $rq->start_date;
		$province_id = $rq->province_id;
		District::create([
			'code' => $code,
			'fullname' => $fullname,
			'start_date' =>$start_date,
			'province_id' =>$province_id
		]);
		return redirect()->route("district.view_all");
	}
	public function update_view($id)
	{
		$district = district::find($id);
		$array_province = Province::get();
		
		return view("pages.districts.update_view",
			[
				"district"=>$district,
				"array_province"=>$array_province
			]);
	}
	public function update_process($id, DistrictRequest $rq)
	{
		district::find($id)->update($rq->all());
		return redirect()->route("district.view_all");
	}
	public function delete()
	{
		$id = $_GET['id'];
		district::find($id)->delete();
		return back();
	}
	
}
