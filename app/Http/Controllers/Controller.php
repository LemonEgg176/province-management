<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Province;
use App\Models\District;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Session;
// use UxWeb\SweetAlert\SweetAlert; 
use Illuminate\Support\Facades\Auth;


class Controller extends BaseController
{
    public function login_process(Request $rq)
    {
        // dd($rq->all());
        // $password = Hash::make($request->newPassword)

        $login = $rq->only('email','password');
        // dd($login);
        if (Auth::attempt($login)) {
            return redirect()->intended('pages/index');
        }else{
            // dd($login);
            return redirect()->route('login');
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect()->route("login");
    }
}
