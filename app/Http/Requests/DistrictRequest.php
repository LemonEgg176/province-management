<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DistrictRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = collect(request()->segments())->last();
        return [
            'code' => '
            bail|
            unique:cs_category_district,code,'.$id.'|
            required|numeric|
            min:10000|max:99999',
            'fullname' => 'required|string',
            'start_date' => 'required|date',
            'province_id' => 'required|exists:cs_category_province,code',
        ];
    }
    public function messages()
    {
        return[
            'code.min' => 'District code must have 5 numbers',
            'code.max' => 'District code must have 5 numbers',
            'required' => 'Please fill :attribute in here',
            'exists' => "This :attribute doesn't exist",
            'code.unique' => "This code already has in database"
        ];
    }
    public function attributes()
    {
        return[
            'fullname' => 'Full name',
            'code' => 'District code',
            'start_date' => 'Start date',
            'province_id' => 'Province code'
        ];
    }
}
