<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Province;
use Illuminate\Http\Request;

class ProvinceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = collect(request()->segments())->last();
        return [
            'code' =>
            'bail|required|
            unique:cs_category_province,code,'.$id.',id|
            numeric|min:100|max:999',
            'fullname' => 'required|string',
            'start_date' => 'required|date',
        ];
    }
    public function messages()
    {
        return[
            'code.min' => 'Province code must have 3 numbers',
            'code.max' => 'Province code must have 3 numbers',
            'required' => 'Please fill :attribute in here',
            'code.unique' => "This code already has in database"
        ];
    }
    public function attributes()
    {
        return[
            'fullname' => 'Full name',
            'code' => 'Province code',
            'start_date' => 'Start date',
        ];
    }
}
