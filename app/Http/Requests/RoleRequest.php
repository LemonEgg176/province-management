<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'description' => 'required|string',
            'permission' => 'required|array'
        ];
    }
    public function message()
    {
        return[
            'permission.required' => 'You must choose at least 1 permission',
            'required' => 'Please fill :attribute in here',
            'name.min' => 'Role name must have at least 3 characters ',
        ];
    }
    public function attributes()
    {
        return[
            'name' => 'Name',
            'description' => 'Description',
            'permission' => 'Permission',
        ];
    }
}

