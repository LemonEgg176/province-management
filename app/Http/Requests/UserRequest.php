<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = collect(request()->segments())->last();
        return [
            'name'=>'required|string',
            'email'
            =>'bail|required|unique:users,email,'.$id.'|email',
            'password'=>'string|min:6',
            'province'=>'required',
            'district'=>'required',
            'role'=>'required'
        ];
    }
    public function messages()
    {
        return[
            'role.required' => 'Choose 1 role',
            'required' => 'Please fill :attribute in here',
            'email.unique' => "This email has already in database",
            'password.min' => "Password must have at least 6 characters",
        ];
    }
}
