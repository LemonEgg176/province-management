
@extends('layouts.master')

@section('title', 'Provinces Mangement')

@section('styles')
  <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
  <link rel="stylesheet" href="{{asset('css/dropdown_search.css')}}">
@endsection
   
@section('nav_bar')
    @include('partials.districts_nav_bar')
@endsection
@section('sidebar')
    @parent
@endsection

@section('content')
@can('create', App\Models\District::class)
  <a href="{{route('district.insert_view')}}">
    <button style="width: 70px; float: right;" class="btn btn-block btn-primary">
      Insert
    </button>
  </a>
@endcan  
	<table id="district" class="table table-bordered table-striped">

		<thead>
			<tr>
				<td>Id</td>
				<td>Code</td>
				<td>Full Name</td>
				<td>Start Date</td>
				<td>Created at</td>
				<td>Updated at</td>
				<td>Province</td>
				<td colspan="2">Action</td>
			</tr>
		</thead>
		<tbody>
    

      @foreach($array_district as $district)
      <tr>
        <td>{{ $district->id }}</td>
        <td>{{ $district->code }}</td>
        <td>{{ $district->fullname }}</td>
        <td>{{ date('d/m/Y',strtotime($district->start_date)) }}</td>
        <td>{{ $district->created_at->format('H:i:s d.m.Y') }}</td>
        <td>{{ $district->deleted_at?$district->deleted_at->format('H:i:s d.m.Y'):'' }}</td>
        
        <td>{{ $district->province?$district->province->fullname:'' }}
        </td>
        <td>
          @can('edit', App\Models\District::class)
            <a href="{{route('district.update_view',['id' => $district->id])}}">Update
            </a>
          @endcan
        </td>

        <td>
          @can('delete', App\Models\District::class)
          <button class="btn btn-primary del" 
             id={{$district->id}}>
              Delete
          </button>
          @endcan
        </td>
      </tr>
      @endforeach
		</tbody>
	</table>

  <div class="pagination">
      {{ $array_district->appends(request()->input())->links() }}
  </div>
@endsection
@section('scripts')
    <script>
    $( ".del" ).click(function() {
      var id = this.id;
      console.log(id);
      Swal.fire({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: 'green',
              cancelButtonColor: '#d33',
              confirmButtonText: '<a href="#" id="delete-btn">Yes, delete it</a>'
            }).then((result) => {
              if (result.isConfirmed) {

                Swal.fire(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                window.location.href="./delete?id="+id;
              }
            })
      });
  </script>
  <script>
    function DropdownSearch() {
      document.getElementById("search_block").style.display='block';
    }
    function exit_dropdown() {
      document.getElementById('search_block').style.display='none';
    }
  </script>
@endsection