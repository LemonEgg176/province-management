@extends('layouts.master')

@section('title', 'User Management')

@section('styles')
  <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
  <link rel="stylesheet" href="{{asset('css/dropdown_search.css')}}">
@endsection

@section('nav_bar')
  @include('partials.user_nav_bar')
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
  @can('create', App\Models\User::class)
    <a href="{{route('user.insert_view')}}">
        <button style="width: 70px; float: right;" class="btn btn-block btn-primary">
          Insert
        </button>
      </a>
  @endcan
  <table id="user" class="table table-bordered table-striped">
    <thead>
      <tr>
        <td>Id</td>
        <td>Name</td>
        <td>Email</td>
        <td>District</td>
        <td>Province</td>
        <td>Role</td>
        <td colspan="2">Action</td>
      </tr>
    </thead>
    <tbody>
     @foreach($users as $user)
      <tr>
        <td>{{ $user->id }}</td>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>
          {{ $user->district?$user->district->fullname:'' }}
        </td>
        <td>
          {{ $user->province?$user->province->fullname:'' }}
        </td>
        <td>
          @foreach ($user->roles as $role)
            {{ $role->name }}
          @endforeach
        </td>
        <td>
          @can('create', App\Models\User::class)
            <a href="{{route('user.update_view',['id' => $user->id])}}">Update
            </a>
          @endcan
        </td>
        <td>
          @can('create', App\Models\User::class)
            <button class="btn btn-primary del" 
             id={{$user->id}}>
             Delete
            </button>
          @endcan
        </td>
      </tr>
     @endforeach
    </tbody>
  </table>
  <div class="pagination">
      {{ $users->appends(request()->input())->links() }}
    </div>
@endsection

@section('scripts')
<script>
    $( ".del" ).click(function() {
      var id = this.id;
      console.log(id);
      Swal.fire({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: 'green',
              cancelButtonColor: '#d33',
              confirmButtonText: '<a href="#" id="delete-btn">Yes, delete it</a>'
            }).then((result) => {
              if (result.isConfirmed) {

                Swal.fire(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                window.location.href="./delete?id="+id;
              }
            })
      });
  </script>
 <script>
    $(document).ready(function () {
      $('#province_search').change(function () {
        let url = "get_district";

          var select = $(this).attr("id");
          var value = $(this).val();
          var dependent = $(this).data('dependent');
          var _token = $("input[name='_token']").val();
          console.log(value);
          $.ajax({
            url:url,
            method:"get",
            data:{
               value:value
            },
            success:function (result) {
              $('#district_search').empty();
              $('#district_search').append('<option value="" disable="true" selected="true">-- Choose Districts --</option>');
                console.log(district_search);
              $.each(result, function(index,districtObj){
                $('#district_search').append("<option value='"+districtObj.code + "'>"
              + districtObj.fullname +
            "</option>")
                // console.log(district);
              }); 
            }
          });
      });
    });
  </script>
{{-- Dropdown search --}}
  <script>
    function DropdownSearch() {
      document.getElementById("search_block").style.display='block';
    }
    function exit_dropdown() {
      document.getElementById('search_block').style.display='none';
    }
  </script>
@endsection
