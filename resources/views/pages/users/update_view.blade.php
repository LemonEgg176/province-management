@extends('layouts.master')

@section('title', 'User Update')

@section('nav_bar')
  @include('partials.nav_bar')
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
<a href="{{route('user.view_all')}}">
    <button style="width: 70px;" class="btn btn-block bg-gradient-secondary">
        Back
    </button>
</a>
  <form role="form" method="post" action="{{route('user.update_process',['id' => $user->id])}}">
     {{csrf_field()}}
    <div class="card-body">
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" name="name" value="{{$user->name}}" placeholder="Enter Name">
        @error('name')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="email">Email</label>
        <input type="text" class="form-control" name="email" value="{{$user->email}}" placeholder="Enter Email">
        @error('email')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>

      <div class="form-group">
        <label for="province">Province</label>
        <br>
        <select class="custom-select" name="province" id="province" data-dependent="district">
          @foreach($array_province as $province)
            <option value="{{$province->code}}" 
              @if($province->code == $user->province_id)
              selected
              @endif>
              {{$province->fullname}}
            </option>
          @endforeach

        </select>
        @error('province')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="district">District</label>
        <br>
        <select class="custom-select" name="district" id="district">
           @foreach($array_district as $district)
            <option value="{{$district->code}}" 
              @if($district->code == $user->district_id)
              selected
              @endif>
              {{$district->fullname}}
            </option>
          @endforeach
        </select>
       
        {{-- <select name="district" id="district">
          <option value="" disabled selected>
            
          </option>
        </select> --}}
        @error('district')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="role">Role</label>
        @foreach($roles as $role)
        <br>
          <input type="radio" name="role"
            @if($role->id == $user_role)
              checked="true" 
            @endif
          value="{{ $role->id }}">
          {{ $role->name }}
        @endforeach
        @error('role')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
@endsection
@section('scripts')
  <script>
    $(document).ready(function () {
      $('#province').change(function () {
        let url = "../get_province_update";
        
          var select = $(this).attr("id");
          var value = $(this).val();
          var dependent = $(this).data('dependent');
          var _token = $("input[name='_token']").val();
          console.log(value);
          $.ajax({
            url:url,
            method:"post",
            data:{
               value,
               _token
            },
            success:function (result) {
              $('#district').empty();
              $('#district').append('<option value="" disable="true" selected="true">-- Choose Districts --</option>');
                console.log(district);
              $.each(result, function(index,districtObj){
                $('#district').append("<option value='"+districtObj.code + "'>"
              + districtObj.fullname +
            "</option>")
                // console.log(district);
              });
            }
          });
      });
    });
  </script>
@endsection