@extends('layouts.master')

@section('title', 'Index')

@section('styles')
	<link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
@endsection
<div class="wrapper">
	 
@section('nav_bar')
    @include('partials.nav_bar')
@endsection
@section('sidebar')
    @parent
@endsection

@section('content')
@endsection

</div>
@section('scripts')
    <!-- jQuery Mapael -->
	<script src="{{asset('plugins/jquery-mousewheel/jquery.mousewheel.js')}}"></script>
	<script src="{{asset('plugins/raphael/raphael.min.js')}}"></script>
	<script src="{{asset('plugins/jquery-mapael/jquery.mapael.min.js')}}"></script>
@endsection
