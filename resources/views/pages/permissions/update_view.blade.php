@extends('layouts.master')

@section('title', 'Permission Update')

@section('nav_bar')
  @include('partials.nav_bar')
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
<a href="{{route('permission.view_all')}}">
    <button style="width: 70px;" class="btn btn-block bg-gradient-secondary">
        Back
    </button>
</a>
  <form role="form" method="post" action="{{route('permission.update_process',['id' => $permission->id])}}">
     {{csrf_field()}}
    <div class="card-body">
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" name="name" value="{{$permission->name}}" placeholder="Enter Name">
        @error('name')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="description">Description</label>
        <textarea name="description" style="resize: none;height: 150px;" class="form-control" placeholder="Enter description">{{$permission->description}}</textarea>
        @error('description')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
@endsection
@section('scripts')
@endsection