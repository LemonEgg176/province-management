
@extends('layouts.master')

@section('title', 'Permission Mangement')

@section('styles')
	<link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
	<link rel="stylesheet" href="{{asset('css/dropdown_search.css')}}">
@endsection
	 
@section('nav_bar')
    @include('partials.permission_nav_bar')
@endsection
@section('sidebar')
    @parent
@endsection

@section('content')
	@can('create', App\Models\Permission::class)
	  <a href="{{route('permission.insert_view')}}">
	    <button style="width: 70px; float: right;" class="btn btn-block btn-primary">
	      Insert
	    </button>
	  </a>
	@endcan
<table id="permission" class="table table-bordered table-hover">
	<thead>
		<tr>
			<td>Id</td>
			<td>Name</td>
			<td>Description</td>
			<td>Created by</td>
			<td>Updated by</td>
			<td>Created at</td>
			<td colspan="2">Action</td>
		</tr>
	</thead>
	<tbody>
		@foreach($permissions as $permission)
		<tr>
			<td>{{$permission->id}}</td>
			<td>{{$permission->name}}</td>
			<td>{{$permission->description}}</td>
			<td>{{$permission->user->name}}</td>
			<td>{{$permission->user_update?
				$permission->user_update->name:''}}</td>
    		<td>
    			{{$permission->created_at}}
    		</td>
			<td>
				@can('edit', App\Models\Permission::class)
					<a href="{{route('permission.update_view',['id' => $permission->id])}}">Update</a>
				@endcan
			</td>
			<td>
				@can('delete', App\Models\Permission::class)
					<button class="btn btn-primary del" 
			             id={{$permission->id}}>
			              Delete
		          	</button>
				@endcan
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
<div>
	{{ $permissions->appends(request()->input())->links() }}
</div>
@endsection

@section('scripts')
	<script>
		function DropdownSearch() {
		  document.getElementById("search_block").style.display='block';
		}
		function exit_dropdown() {
		  document.getElementById('search_block').style.display='none';
		}
	</script>
	<script>
    $( ".del" ).click(function() {
      var id = this.id;
      console.log(id);
      Swal.fire({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: 'green',
              cancelButtonColor: '#d33',
              confirmButtonText: '<a href="#" id="delete-btn">Yes, delete it</a>'
            }).then((result) => {
              if (result.isConfirmed) {

                Swal.fire(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                window.location.href="./delete?id="+id;
              }
            })
      });
  </script>
@endsection
