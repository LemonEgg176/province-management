
@extends('layouts.master')

@section('title', 'Provinces Mangement')

@section('styles')
	<link rel="stylesheet" href="{{asset('css/dropdown_search.css')}}">
	<link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
@endsection
	 
@section('nav_bar')
    @include('partials.province_nav_bar')
@endsection
@section('sidebar')
    @parent
@endsection

@section('content')
	@can('create', \App\Models\Province::class)
	  <a href="{{route('province.insert_view')}}">
	    <button style="width: 70px; float: right;" class="btn btn-block btn-primary">
	      Insert
	    </button>
	  </a>
	@endcan
<table id="province" class="table table-bordered table-hover">
	<thead>
		<tr>
			<td>Id</td>
			<td>Code</td>
			<td>Full Name</td>
			<td>Start Date</td>
			<td>Created at</td>
			<td>Updated at</td>
			<td colspan="2">Action</td>
		</tr>
	</thead>
	<tbody>
		@foreach($array_province as $province)
		<tr>
			<td>{{$province->id}}</td>
			<td>{{$province->code}}</td>
			<td>{{$province->fullname}}</td>
			<td>{{ date('d/m/Y',strtotime($province->start_date)) }}</td>
			<td>{{ $province->created_at->format('H:i:s d.m.Y') }}</td>
    		<td>{{ $province->deleted_at?$province->updated_at->format('H:i:s d.m.Y'):'' }}
    		</td>
			<td>
				@can('edit',\App\Models\Province::class)
					<a href="{{route('province.update_view',['id' => $province->id])}}">Update</a>
				@endcan
			</td>
			<td>
				@can('delete',\App\Models\Province::class) 
					@if($province->del)
						<button class="btn btn-primary del" 
							id={{$province->id}}>
							Delete
						</button>
					@else
						<button class="btn btn-primary" 
								onclick="
								notDelete()
							">
							Delete
						</button>
					@endif 
				@endcan
				
			</td>
		</tr>
		@endforeach
	</tbody>

</table>
<div>
	{{ $array_province->appends(request()->input())->links() }}
</div>
@endsection

@section('scripts')
	<script>
		function DropdownSearch() {
		  document.getElementById("search_block").style.display='block';
		}
		function exit_dropdown() {
		  document.getElementById('search_block').style.display='none';
		}
	</script>
	@if(!empty($array_province->items()))
	<script>
		function notDelete() {
			Swal.fire("You can't delete this field")
		}
	</script>
	@endif
	<script>
		$( ".del" ).click(function() {
		  var id = this.id;
		  console.log(id);
		  Swal.fire({
				  title: 'Are you sure?',
				  text: "You won't be able to revert this!",
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: 'green',
				  cancelButtonColor: '#d33',
				  confirmButtonText: '<a href="#" id="delete-btn">Yes, delete it</a>'
				}).then((result) => {
				  if (result.isConfirmed) {
	
					Swal.fire(
					  'Deleted!',
					  'Your file has been deleted.',
					  'success'
					)
					window.location.href="./delete?id="+id;
				  }
				})
		  });
	  </script>
@endsection
