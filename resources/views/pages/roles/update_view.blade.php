@extends('layouts.master')

@section('title', 'Role Update')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/permission_tree.css') }}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
@endsection
@section('nav_bar')
  @include('partials.nav_bar')
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
<a href="{{route('role.view_all')}}">
    <button style="width: 70px;" class="btn btn-block bg-gradient-secondary">
        Back
    </button>
</a>
  <form role="form" method="post" action="{{route('role.update_process',['id' => $role->id])}}">
     {{csrf_field()}}
    <div class="card-body">
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" name="name" value="{{$role->name}}" placeholder="Enter Name">
        @error('name')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="description">Description</label>
        <textarea name="description" style="resize: none;height: 150px;" class="form-control" placeholder="Enter description">{{$role->description}}</textarea>
        @error('description')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      
      <div class="form-group">
        <label for="permission">
          Role's permissions
        </label>
        <ul id="permission_ul">
         
        @foreach($parent_permission as $pp)
          <li id="{{ $pp->parent}}">
            <i class="fa">&#xf0da;</i>
            @php
              $checked = 'checked';
               foreach($permissions as $value){
                if($value->parent == $pp->parent){
                   if(!in_array($value->id , $role_permissions->pluck('permission_id')->toArray())){
                    $checked = '';
                   }
                }
              }
            @endphp
            <input type="checkbox" class="parent_checkbox" 
            id="parent-{{$pp->parent}}" name="parent_name"
            data-parent = "{{ $pp->parent }}"
            {{  $checked }}  
            >
            {{ $pp->parent_name }}
            <ul class="nested">
              @foreach($permissions as $permission)
                @if($permission->parent == $pp->parent)
                  <li class="child-{{$pp->parent_name}}">
                    <input type="checkbox" 
                    name="permission[]"
                    class ="children"
                    id="child-{{ $permission->id }}"
                      data-children = "{{ $permission->parent }}"
                      @foreach($role_permissions as $role_permission )
                        @if($permission->id == $role_permission->permission_id)
                          checked
                        @endif
                      @endforeach
                    value="{{ $permission->id }}">
                    {{ $permission->name }}
                  </li>
                @endif
              @endforeach
            </ul>
          </li>
        @endforeach
        </ul>
        @error('permission')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
    </div>

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
@endsection
@section('scripts')
<script>
var toggler = document.getElementsByClassName("fa");
var i;
for (i = 0; i < toggler.length; i++) {
  toggler[i].addEventListener("click", function() {
    this.parentElement.querySelector(".nested").classList.toggle("active");
    // this.classList.toggle("caret-down");
  });
}
</script>
<script>
  $(document).ready(function() {
    $(".parent_checkbox").click(function(){
      var $parentData = $(this).data("parent");
        if($(this).is(":checked")){
        $('#'+$parentData).find(".children").prop("checked",true);
      }else{
         $('#'+$parentData).find(".children").prop("checked",false);
      }
    });
  });
</script>
<script>
  $(document).ready(function() {
    $(".children").click(function(){
      var $childrenData = $(this).data("children");
      var $a = $('#'+$childrenData).find(".children");
      var flag = true
      for (var $i = 0; $i < $a.length; $i++) {
        if($($a[$i]).is(":checked") == false){
          flag = false
          break;
        }
      }
      $('#parent-'+$childrenData).prop("checked",flag);
    });
  });
</script>
@endsection