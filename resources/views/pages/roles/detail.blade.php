
@extends('layouts.master')

@section('title', 'Role Detail')

@section('styles')
<style>
  ul, #permission_ul{
  list-style-type: none;
}
 ul, .nested{
  list-style-type: circle;
}
.fa{
  cursor: pointer;
}
.fa{
  cursor: pointer;
  -webkit-user-select: none; /* Safari 3.1+ */
  -moz-user-select: none; /* Firefox 2+ */
  -ms-user-select: none; /* IE 10+ */
  user-select: none;
}
.nested{
  display: none
}
.active {
  display: block;
}
</style>
@endsection
	 
@section('nav_bar')
    @include('partials.nav_bar')
@endsection
@section('sidebar')
    @parent
@endsection

@section('content')
<a href="{{route('role.view_all')}}">
    <button style="width: 70px;" class="btn btn-block bg-gradient-secondary">
        Back
    </button>
</a>
    <div class="card-body">
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" readonly="" class="form-control" name="name" value="{{$role->name}}">
      </div>
      <div class="form-group">
        <label for="description">Description</label>
        <textarea name="description" style="resize: none;height: 150px;" class="form-control" readonly="true">{{$role->description}}</textarea>
      </div>
      <div class="form-group">
        <label for="permission">
          Role's permissions
        </label>
        <ul id="permission_ul">
          @foreach($unique_parent as $u_par)
            <li>
              <i class="fa">&#xf0da;</i>
                @foreach($parent_permission as $pp)
                  @if ($u_par == $pp->parent)
                    <span>{{ $pp->parent_name }}</span>
                  @endif
                @endforeach
              <ul class="nested">
                @foreach ($permissions as $p)
                        @if($u_par == $p->parent)
                          <li>
                            {{$p->name}}
                          </li>
                        @endif
                @endforeach
              </ul>
            </li>
          @endforeach
        </ul>  
      </div>
    </div>
@endsection

@section('scripts')
  <script>
  var toggler = document.getElementsByClassName("fa");
  var i;
  for (i = 0; i < toggler.length; i++) {
    toggler[i].addEventListener("click", function() {
      this.parentElement.querySelector(".nested").classList.toggle("active");
      // this.classList.toggle("caret-down");
    
    });
  }
  </script>
@endsection