
@extends('index')

@section('content')

    <section class="content">
      <a href="{{route('province.insert_view')}}">
        <button style="width: 70px; float: right;" class="btn btn-block btn-primary">
          Insert
        </button>
      </a>
	<table id="province" class="table table-bordered table-hover">
		<thead>
			<tr>
				<td>Id</td>
				<td>Code</td>
				<td>Full Name</td>
				<td>Start Date</td>
				<td>Created at</td>
				<td>Updated at</td>
				<td colspan="2">Action</td>
			</tr>
		</thead>
		<tbody>
			@foreach($array_province as $province)
			<tr>
				<td>{{$province->id}}</td>
				<td>{{$province->code}}</td>
				<td>{{$province->fullname}}</td>
				<td>{{ date('d/m/Y',strtotime($province->start_date)) }}</td>
				<td>{{ $province->created_at->format('H:i:s d.m.Y') }}</td>
        		<td>{{ $province->deleted_at?$province->deleted_at->format('H:i:s d.m.Y'):'' }}</td>
				<td><a href="{{route('province.update_view',['id' => $province->id])}}">Update</a></td>
				<td>
					
						<button class="btn btn-primary" 
						onclick="
							@if($province->del)
								deleteBtn()
							@else
								notDelete()
							@endif 
						">
						Delete
					</button>
				</td>
			</tr>
			@endforeach
		</tbody>
	
	</table>
	<div>
		{{ $array_province->appends(request()->input())->links() }}
    </div>

    </section>

@if(!empty($array_province->items()))
<script>
	function deleteBtn() {
		Swal.fire({
	          title: 'Are you sure?',
	          text: "You won't be able to revert this!",
	          icon: 'warning',
	          textColor: 'white',
	          showCancelButton: true,
	          confirmButtonColor: 'green',
	          cancelButtonColor: '#d33',
	          confirmButtonText: '<a href="{{route('province.delete',['id' => $province->id])}}" id="delete-btn">Yes, delete it</a>'
	        }).then((result) => {
	          if (result.isConfirmed) {
	            Swal.fire(
	              'Deleted!',
	              'Your file has been deleted.',
	              'success'
	            )
	          }
	        })
		}
	function notDelete() {
		Swal.fire("You can't delete this field")
	}
</script>
@endif
@endsection