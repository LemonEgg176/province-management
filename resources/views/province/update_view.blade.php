@extends('index')

@section('content')
<a href="{{route('province.view_all')}}">
    <button style="width: 70px;" class="btn btn-block bg-gradient-secondary">
        Back
    </button>
</a>
  <form role="form" method="post" action="{{route('province.update_process',['id' => $province->id])}}">
     {{csrf_field()}}
    <div class="card-body">
      <div class="form-group">
        <label for="code">Code</label>
        <input type="text" class="form-control" name="code" value="{{$province->code}}">
        @error('code')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="fullname">Full Name</label>
        <input type="text" class="form-control" name="fullname" value="{{$province->fullname}}">
        @error('fullname')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="start_date">Start Date</label>
        <input type="date" class="form-control" name="start_date" value="{{$province->start_date}}">
        @error('start_date')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Update</button>
    </div>
  </form>
 
@endsection