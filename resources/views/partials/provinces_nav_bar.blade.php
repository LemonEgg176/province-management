<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{route('index')}}" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{route('logout')}}" class="nav-link">Logout</a>
      </li>
    </ul>
    
    <!-- SEARCH FORM -->
    <div class="dropdown_search">
      <form action="" class="form-inline ml-3">
        <div class="input-group input-group-sm">
          @if(isset($_GET['name_search']))
           <input class="form-control form-control-navbar" id="search" type="search" name="name_search" placeholder="Search by name" aria-label="Search" value="{{ $_GET['name_search'] }}">
          @elseif(empty($_GET['name_search']))
            <input class="form-control form-control-navbar" id="search" type="search" name="name_search" placeholder="Search by name" aria-label="Search">
          @endif
          <div class="input-group-append">
            <button class="btn btn-navbar" type="submit">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form>
    </div>   
  </nav>
</div>