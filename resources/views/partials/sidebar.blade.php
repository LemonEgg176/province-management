<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('index')}}" class="brand-link">
      <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Apsrise</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          name
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          @if (Auth::user()->can('showProvince',App\Models\Province::class) || Auth::user()->can('showDistrict', App\Models\District::class))
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-circle"></i>
              <p>
                Categories
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                @can('showProvince', App\Models\Province::class)
                  <a href="{{route('province.view_all')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Provinces</p>
                  </a>
                @endcan
              </li>
              <li class="nav-item">
                @can('showDistrict', App\Models\District::class)
                  <a href="{{route('district.view_all')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Districts</p>
                  </a>
                @endcan
              </li>
            </ul>
          </li>
          @endif
          @if (Auth::user()->can('show', App\Models\User::class) || Auth::user()->can('show', App\Models\Role::class) || Auth::user()->can('showPermission',App\Models\Permission::class))
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-circle"></i>
              <p>
                System
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                @can('show', App\Models\User::class)
                  <a href="{{route('user.view_all')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Users</p>
                </a>
                @endcan
              </li>
              <li class="nav-item">
                @can('show', App\Models\Role::class)
                  <a href="{{route('role.view_all')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Roles</p>
                </a>
                @endcan
              </li>
              <li class="nav-item">
                @can('showPermission',App\Models\Permission::class)
                  <a href="{{route('permission.view_all')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Permissions</p>
                </a>
                @endcan
              </li>
            </ul>
          </li>
          @endif
        </ul>
      </nav>
    </div>
  </aside>