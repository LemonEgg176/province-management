<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
      <a href="{{route('index')}}" class="nav-link">Home</a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
      <a href="{{route('logout')}}" class="nav-link">Logout</a>
    </li>
  </ul>
  <div>
     @can('search',App\Models\District::class)
    <button onclick="DropdownSearch()" class="btn btn-block btn-primary">Search</button>

    <!-- SEARCH FORM -->
   
    <div class="dropdown_search" id="search_block">
      <div>
        <span onclick="exit_dropdown()"
          id="close" title="Close Modal">&times;
        </span>
      </div>
      <div class="card-body">
        <form action="" class="form-inline ml-3">
          <div>
            <div class="form-group">
              <label for="province_search">Province</label>
            </div>
            <div>
              <select class="custom-select" name="province_search" id="province_search">
                @if(isset($_GET['province_search']))
                <option value="">None</option>
                @foreach($array_province as $province)
                  <option value="{{$province->code}}" 
                    @if($province->code 
                      == $_GET['province_search'])
                        selected
                    @endif
                    >
                    {{$province->fullname}}
                  </option>
                @endforeach
                @elseif(empty($_GET['province_search']))
                  <option value="" disabled selected>Choose Province</option>
                  @foreach($array_province as $province)
                  <option value="{{$province->code}}">
                    {{$province->fullname}}
                  </option>
                  @endforeach
                @endif
              </select>
            </div>
            <div class="form-group">
              <label for="name_search">Name</label>
            </div>
            <div>
            <input class="form-control" id="search" type="search" name="name_search" placeholder="Search by name" aria-label="Search" value="{{Request::get('name_search')}}">
            </div>
          
            <div class="form-group">
              <label>From</label>
            </div>
            <div>
              <input class="form-control" type="date" name="date_from"
              value={{Request::get('date_from')}} >
            </div>
          
            <div class="form-group">
              <label>To</label>
            </div>
            <div>
              <input class="form-control datetimepicker-input" type="date" name="date_to"
              value={{Request::get('date_to')}} >
            </div>
            <div class="form-group">
              <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
                <span>Search</span>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
    @endcan 
  </div>
</nav>