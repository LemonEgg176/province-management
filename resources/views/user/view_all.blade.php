
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>AdminLTE 3 | Dashboard 2</title>
  {{-- Sweet Alert --}}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
  <!-- Font Awesome Icons -->

  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  {{-- Sweet alert --}}
<link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.js"></script>
{{-- jQuery --}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<style>
  .w-5{
    height: 10px !important;
  }
  #delete-btn{
    color: white;
  }
</style>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{route('index')}}" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{route('logout')}}" class="nav-link">Logout</a>
      </li>
    </ul>
    
    <!-- SEARCH FORM -->
    <div class="dropdown_search">
      <form action="" class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <label for="name_search">Name</label>
         <input class="form-control form-control-navbar" id="search" type="search" name="name_search" placeholder="Search by name" aria-label="Search" value="{{Request::get('name_search')}}">
         <select name="province_search" class="custom-select" id="province_search">
          @if(isset($_GET['province_search']))
          <option value="">None</option>
          @foreach($array_province as $province)
            <option value="{{$province->code}}" 
              @if($province->code 
                == $_GET['province_search'])
                  selected
              @endif
              >
              {{$province->fullname}}
            </option>
          @endforeach
          @elseif(empty($_GET['province_search']))
            <option value="" disabled selected>Choose Province</option>
            @foreach($array_province as $province)
            <option value="{{$province->code}}">
              {{$province->fullname}}
            </option>
            @endforeach
           @endif
        </select>
       <select name="district_search" class="custom-select" id="district_search">
          @if(isset($_GET['district_search']))
          <option value="">None</option>
           @foreach($array_district as $district)
            <option value="{{$district->code}}" 
              @if($district->code == $_GET['district_search'])
              selected
              @endif>
              {{$district->fullname}}
            </option>
          @endforeach
          @elseif(empty($_GET['district_search']))
            <option value="" disabled selected>
              Choose Province First
            </option>
           @endif
        </select>
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>
    </div>
    <!-- Right navbar links --> 
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('index')}}" class="brand-link">
      <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Apsrise</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
       
        <div class="info">
          <a href="#" class="d-block">name</a>
        </div>
      </div>
    
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('province.view_all')}}" class="nav-link">
                  <p>Province</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('district.view_all')}}" class="nav-link">
                  <p>District</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('user.view_all')}}" 
                class="nav-link">
                  <p>User</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <div class="content-wrapper">

    <section class="content">
      <a href="{{route('user.insert_view')}}">
        <button style="width: 70px; float: right;" class="btn btn-block btn-primary">
          Insert
        </button>
      </a>
  <table id="user" class="table table-bordered table-striped">

    <thead>
      <tr>
        <td>Id</td>
        <td>Name</td>
        <td>Email</td>
  
        <td>District</td>
        <td>Province</td>
        <td colspan="2">Action</td>
      </tr>
    </thead>
    <tbody>
    

     @foreach($users as $user)
      <tr>
        <td>{{ $user->id }}</td>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>

        <td>
          {{ $user->district?$user->district->fullname:'' }}
        </td>
        <td>
          {{ $user->province?$user->province->fullname:'' }}
        </td>
        <td>
          <a href="{{route('user.update_view',['id' => $user->id])}}">Update
          </a>
        </td>
        <td>
          <button class="btn btn-primary" onclick="Delete()">
            Delete
          </button> 
        </td>
      </tr>
     
     @endforeach

    </tbody>
  </table>
  <div class="pagination">
      {{ $users->appends(request()->input())->links() }}
    </div>
</section>
    
  </div>

</div>
@if(!empty($users->items()))
  <script>
  function Delete() {
    Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          textColor: 'white',
          showCancelButton: true,
          confirmButtonColor: 'green',
          cancelButtonColor: '#d33',
          confirmButtonText: '<a href="{{route('user.delete',['id' => $user->id])}}">Yes, delete it</a>'
        }).then((result) => {
          if (result.isConfirmed) {
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
          }
        })
  }
  </script>
@endif
 <script>
    $(document).ready(function () {
      $('#province_search').change(function () {
        let url = "get_district";

          var select = $(this).attr("id");
          var value = $(this).val();
          var dependent = $(this).data('dependent');
          var _token = $("input[name='_token']").val();
          console.log(value);
          $.ajax({
            url:url,
            method:"get",
            data:{
               value:value
            },
            success:function (result) {
              $('#district_search').empty();
              $('#district_search').append('<option value="" disable="true" selected="true">-- Choose Districts --</option>');
                console.log(district_search);
              $.each(result, function(index,districtObj){
                $('#district_search').append("<option value='"+districtObj.code + "'>"
              + districtObj.fullname +
            "</option>")
                // console.log(district);
              });
              
            }
          });
        
      });
     
    });
   
  </script>
<!-- jQuery -->

<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap -->

<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- AdminLTE App -->

<script src="{{asset('dist/js/adminlte.js')}}"></script>
<!-- OPTIONAL SCRIPTS -->

<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- jQuery Mapael -->

<script src="{{asset('plugins/jquery-mousewheel/jquery.mousewheel.js')}}"></script>

<script src="{{asset('plugins/raphael/raphael.min.js')}}"></script>

<script src="{{asset('plugins/jquery-mapael/jquery.mapael.min.js')}}"></script>

<script src="{{asset('plugins/jquery-mapael/maps/usa_states.min.js')}}"></script>
<!-- PAGE SCRIPTS -->
<script src="{{asset('dist/js/pages/dashboard2.js')}}"></script>
</body>
</html>
