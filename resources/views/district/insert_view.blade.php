@extends('index')

@section('content')
<a href="{{route('district.view_all')}}">
    <button style="width: 70px;" class="btn btn-block bg-gradient-secondary">
        Back
    </button>
</a>
  <form role="form" method="post" action="{{route('district.insert_process')}}">
     {{csrf_field()}}
    <div class="card-body">
      <div class="form-group">
        <label for="code">Code</label>
        <input type="text" class="form-control" name="code" value="{{old('code')}}" placeholder="Enter code">
        @error('code')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="fullname">Full Name</label>
        <input type="text" class="form-control" name="fullname" value="{{old('fullname')}}" placeholder="Enter full name">
        @error('fullname')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="start_date">Start Date</label>
        <input  type="date" class="form-control" name="start_date" value="{{old('start_date')}}" placeholder="Choose date">
         @error('start_date')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="start_date">Province Id</label>
        <select class="custom-select" name="province_id" id="province_id">
          @foreach($array_province as $province)
            <option value="{{$province->code}}">
              {{$province->fullname}}
            </option>
          @endforeach
        </select>
        @error('province_id')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
  
@endsection